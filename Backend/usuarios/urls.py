from django.urls import path,include
from usuarios.views import UsuarioListView,UsuarioLoginView,UsuarioLogin,UsuarioLogin2
urlpatterns = [
    path('', UsuarioListView.as_view()),
    path('login2',UsuarioLogin2.as_view()),
    path('login/<str:username>/<str:password>', UsuarioLogin.as_view()),
]
